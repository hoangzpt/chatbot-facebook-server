const viewEngine = require("./config/viewEngine");
const initWebRoutes =  require('./routes/web');

const express = require('express');
const route = express.Router();
const bodyParser = require('body-parser');
const http = require("http");

require("dotenv").config();

const app = express();
const server = http.createServer(app);
const socketIo = require("socket.io")(server, {
    cors: {
        origin: "*",
    }
});


socketIo.on("connection", (socket) => {
    console.log("New client connected" + socket.id);

    socket.emit("getId", socket.id);

    socket.on("sendDataClient", function(data) {
        console.log(data.content)
        socketIo.emit("sendDataServer", { data });
    })

    socket.on("disconnect", () => {
        console.log("Client disconnected");
    });
});

viewEngine(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( {extended: true} ));

initWebRoutes(app);

const PORT = process.env.PORT;

server.listen(3000, () => {
    console.log(`Server is running on ${PORT}`);
})


// var express = require('express')
// const http = require("http");
// var app = express();
// const server = http.createServer(app);
//
// const socketIo = require("socket.io")(server, {
//     cors: {
//         origin: "*",
//     }
// });
//
//
// socketIo.on("connection", (socket) => {
//     console.log("New client connected" + socket.id);
//
//     socket.emit("getId", socket.id);
//
//     socket.on("sendDataClient", function(data) {
//         console.log(data)
//         socketIo.emit("sendDataServer", { data });
//     })
//
//     socket.on("disconnect", () => {
//         console.log("Client disconnected");
//     });
// });
//
// server.listen(3000, () => {
//     console.log('Server đang chay tren cong 3000');
// });

